/* Performs a linear interpolation to obtain the parameters for the
   self-shielding correction.

   The tabulated fits for the self-shielding correction to the HI
   photo-ionisation rate use Rahmati et al. (2013) from z=0 to z=5,
   and Chardin et al. (2017) from z=6 to z=10.  The redshift bins are
   separated by dz=1.0.  See Table A1 and Eq. A1 in Rahmati et
   al. (2013), and Table A1 in Chardin et al. (2017) */

#include <stdlib.h>
#include <stdio.h>

#include "proto.h"
#include "global_vars.h"

void
SelfShieldFit()
{

  double n0[11] = { 1.148e-3, 5.129e-3, 8.710e-3, 7.413e-3, 5.888e-3, 4.467e-3,
    6.955e-3, 2.658e-3, 3.974e-3, 4.598e-3, 4.694e-3
  };

  double alpha1[11] = { -3.98, -2.94, -2.22, -1.99, -2.05, -2.63,
    -0.941372, -0.866887, -0.742237, -0.642645, -0.386421
  };

  double alpha2[11] = { -1.09, -0.90, -1.09, -0.88, -0.75, -0.57,
    -1.507124, -1.272957, -1.397100, -1.212105, -0.857335
  };

  double beta[11] = { 1.29, 1.21, 1.75, 1.72, 1.93, 1.77,
    6.109438, 7.077863, 7.119987, 9.988387, 12.94204
  };

  double fval[11] = { 0.01, 0.03, 0.03, 0.04, 0.02, 0.01,
    0.028830, 0.040894, 0.041213, 0.028581, 0.005710
  };

  double zmin = 0.0, deltaz = 1.0, flow, fhi, t;
  int j;


  /* Perform linear interpolation on fit parameters */
  t = (redshift - zmin) / deltaz;
  j = (int) t;
  fhi = t - j;
  flow = 1.0 - fhi;

  if (j < 10)
    {
      n0_z = flow * n0[j] + fhi * n0[j + 1];
      alpha1_z = flow * alpha1[j] + fhi * alpha1[j + 1];
      alpha2_z = flow * alpha2[j] + fhi * alpha2[j + 1];
      beta_z = flow * beta[j] + fhi * beta[j + 1];
      fval_z = flow * fval[j] + fhi * fval[j + 1];
    }
  else
    {
      printf("Redshift of snapshot is outside of self-shielding table\n");
      printf("Stopping...\n\n");
      exit(0);
    }

  return;
}
