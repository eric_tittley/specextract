/* Initialise cooling and ionisation table at start of run */

#include <stdio.h>
#include <stdlib.h>

#include "parameters.h"
#include "proto.h"

void
InitCool(void)
{
  char fname[400];

  sprintf(fname, "./%s", UVBFILE);

  InitCoolMemory();
  MakeCoolingTable();

  ReadIonizeParams(fname);

  IonizeParamsTable();
  //  SelfShieldFit();
}
