#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "global_vars.h"
#include "parameters.h"
#include "proto.h"

void
InitLOSMemory()
{

  ilos = (int *) calloc(NUMLOS, sizeof(int));
  if (NULL == ilos)
    {
      free(ilos);
      printf("Memory allocation failed in extract_spectra.c\n");
      exit(0);
    }

  xlos = (double *) calloc(NUMLOS, sizeof(double));
  if (NULL == xlos)
    {
      free(xlos);
      printf("Memory allocation failed in extract_spectra.c\n");
      exit(0);
    }

  ylos = (double *) calloc(NUMLOS, sizeof(double));
  if (NULL == ylos)
    {
      free(ylos);
      printf("Memory allocation failed in extract_spectra.c\n");
      exit(0);
    }

  zlos = (double *) calloc(NUMLOS, sizeof(double));
  if (NULL == zlos)
    {
      free(zlos);
      printf("Memory allocation failed in extract_spectra.c\n");
      exit(0);
    }

  posaxis = (double *) calloc(NBINS, sizeof(double));
  if (NULL == posaxis)
    {
      free(posaxis);
      printf("Memory allocation failed in extract_spectra.c\n");
      exit(0);
    }

  velaxis = (double *) calloc(NBINS, sizeof(double));
  if (NULL == posaxis)
    {
      free(posaxis);
      printf("Memory allocation failed in extract_spectra.c\n");
      exit(0);
    }

  rhoker_H = (double *) calloc(NUMLOS * NBINS, sizeof(double));
  if (NULL == rhoker_H)
    {
      free(rhoker_H);
      printf("Memory allocation failed in extract_spectra.c\n");
      exit(0);
    }

  rhoker_H1 = (double *) calloc(NUMLOS * NBINS, sizeof(double));
  if (NULL == rhoker_H1)
    {
      free(rhoker_H1);
      printf("Memory allocation failed in extract_spectra.c\n");
      exit(0);
    }

  velker_H1 = (double *) calloc(NUMLOS * NBINS, sizeof(double));
  if (NULL == velker_H1)
    {
      free(velker_H1);
      printf("Memory allocation failed in extract_spectra.c\n");
      exit(0);
    }

  tempker_H1 = (double *) calloc(NUMLOS * NBINS, sizeof(double));
  if (NULL == tempker_H1)
    {
      free(tempker_H1);
      printf("Memory allocation failed in extract_spectra.c\n");
      exit(0);
    }

  tau_H1 = (double *) calloc(NUMLOS * NBINS, sizeof(double));
  if (NULL == tau_H1)
    {
      free(tau_H1);
      printf("Memory allocation failed in extract_spectra.c\n");
      exit(0);
    }

#ifdef METALS
  metker = (double *) calloc(NUMLOS * NBINS, sizeof(double));
  if (NULL == metker)
    {
      free(metker);
      printf("Memory allocation failed in extract_spectra.c\n");
      exit(0);
    }
#endif

}
