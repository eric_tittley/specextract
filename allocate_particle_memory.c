/* this routine allocates the memory for the particle data */

#include <stdio.h>
#include <stdlib.h>

#include "global_vars.h"
#include "parameters.h"
#include "proto.h"

/* this routine allocates the memory for the particle data */
void
allocate_particle_memory()
{
  if (!(P = calloc(NumPart_ThisFile[0], sizeof(struct particle_data))))
    {
      fprintf(stderr, "failed to allocate memory.\n\n");
      exit(0);
    }
}
