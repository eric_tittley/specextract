double *rhoker_H, *posaxis, *velaxis;
double *rhoker_H1, *velker_H1, *tempker_H1, *tau_H1;
#ifdef METALS
double *metker;
#endif
double *xlos, *ylos, *zlos;
int *ilos;

double vmax, vmax2, drbin;

char *path, *snapbase;
int snapshot;
