/* Allocate the arrays for the cooling function table */

#include <stdlib.h>
#include <stdio.h>

#include "proto.h"

void
InitCoolMemory(void)
{
  AlphaHp = (double *) calloc((NCOOLTAB + 1), sizeof(double));
  if (NULL == AlphaHp)
    {
      free(AlphaHp);
      printf("Memory allocation failed for AlphaHp.\n");
      exit(0);
    }
  AlphaHep = (double *) calloc((NCOOLTAB + 1), sizeof(double));
  if (NULL == AlphaHep)
    {
      free(AlphaHep);
      printf("Memory allocation failed for AlphaHep.\n");
      exit(0);
    }
  AlphaHepp = (double *) calloc((NCOOLTAB + 1), sizeof(double));
  if (NULL == AlphaHepp)
    {
      free(AlphaHepp);
      printf("Memory allocation failed for AlphaHepp.\n");
      exit(0);
    }
  Alphad = (double *) calloc((NCOOLTAB + 1), sizeof(double));
  if (NULL == Alphad)
    {
      free(Alphad);
      printf("Memory allocation failed for Alphad.\n");
      exit(0);
    }
  GammaeH0 = (double *) calloc((NCOOLTAB + 1), sizeof(double));
  if (NULL == GammaeH0)
    {
      free(GammaeH0);
      printf("Memory allocation failed for GammaeH0.\n");
      exit(0);
    }
  GammaeHe0 = (double *) calloc((NCOOLTAB + 1), sizeof(double));
  if (NULL == GammaeHe0)
    {
      free(GammaeHe0);
      printf("Memory allocation failed for GammaeHe0.\n");
      exit(0);
    }
  GammaeHep = (double *) calloc((NCOOLTAB + 1), sizeof(double));
  if (NULL == GammaeHep)
    {
      free(GammaeHep);
      printf("Memory allocation failed for GammaeHep.\n");
      exit(0);
    }
}
