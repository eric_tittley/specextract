#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "global_vars.h"
#include "parameters.h"
#include "proto.h"

double gJH0, gJHe0, gJHep;

#ifdef SELF_SHIELD
double gJH0_SS;
#endif

double nHp, nHep, nHepp, nH0, nHe0, ne, necgs;
double aHp, aHep, aHepp, ad, geH0, geHe0, geHep;

double n0_z, alpha1_z, alpha2_z, beta_z, fval_z;

double deltaT;
double *AlphaHp, *AlphaHep, *AlphaHepp, *Alphad;
double *GammaeH0, *GammaeHe0, *GammaeHep;
double logTmin, logTmax;


/******************************************************************************************/

/* This function computes the equilibrium abundance ratios */

void
ion_balance(double nHcgs, double logT, double ne_input)
{
  double neold, nenew, yhelium;
  double flow, fhi, t;
  double gJH0ne, gJHe0ne, gJHepne;

  int j, niter;


  yhelium = (1.0 - XH) / (4.0 * XH);


  if (logT <= logTmin)          /* everything neutral */
    {
      nH0 = 1.0;
      nHe0 = yhelium;
      nHp = 0.0;
      nHep = 0.0;
      nHepp = 0.0;
      ne = 0.0;
      return;
    }

  if (logT >= logTmax)          /* everything is ionised */
    {
      nH0 = 0.0;
      nHe0 = 0.0;
      nHp = 1.0;
      nHep = 0.0;
      nHepp = yhelium;
      ne = nHp + 2.0 * nHepp;
      return;
    }


  t = (logT - logTmin) / deltaT;
  j = (int) t;
  fhi = t - j;
  flow = 1 - fhi;


  ne = ne_input;
  neold = ne;
  niter = 0;
  necgs = ne * nHcgs;


  /* Evaluate number densities iteratively in units of nH */
  do
    {
      niter++;

      /* Interpolations from the cooling table */
      /* Recombination rates */
      aHp = flow * AlphaHp[j] + fhi * AlphaHp[j + 1];
      aHep = flow * AlphaHep[j] + fhi * AlphaHep[j + 1];
      aHepp = flow * AlphaHepp[j] + fhi * AlphaHepp[j + 1];
      ad = flow * Alphad[j] + fhi * Alphad[j + 1];

      /* Collisional ionisation rates */
#ifdef NOCOLLISIONAL
      geH0 = 0.0;
      geHe0 = 0.0;
      geHep = 0.0;
#else
      geH0 = flow * GammaeH0[j] + fhi * GammaeH0[j + 1];
      geHe0 = flow * GammaeHe0[j] + fhi * GammaeHe0[j + 1];
      geHep = flow * GammaeHep[j] + fhi * GammaeHep[j + 1];
#endif

      if (necgs <= 1.e-25 || J_UV == 0)
        gJH0ne = gJHe0ne = gJHepne = 0;
      else
        {
#ifdef SELF_SHIELD
          gJH0ne = gJH0_SS / necgs;     /* NOTE: uses RT correction */
#else
          gJH0ne = gJH0 / necgs;
#endif
          gJHe0ne = gJHe0 / necgs;
          gJHepne = gJHep / necgs;
        }

      /* Follow Katz, Weinberg & Hernquist, 1996, ApJS, 105, 19, eq. 33-38 */
      nH0 = aHp / (aHp + geH0 + gJH0ne);
      nHp = 1.0 - nH0;

      if ((gJHe0ne + geHe0) <= SMALLNUM)        /* no ionisation at all */
        {
          nHep = 0.0;
          nHepp = 0.0;
          nHe0 = yhelium;
        }
      else
        {
          nHep = yhelium / (1.0 + (aHep + ad) / (geHe0 + gJHe0ne) + (geHep + gJHepne) / aHepp);
          nHe0 = nHep * (aHep + ad) / (geHe0 + gJHe0ne);
          nHepp = nHep * (geHep + gJHepne) / aHepp;
        }

      neold = ne;
      ne = nHp + nHep + 2.0 * nHepp;
      necgs = ne * nHcgs;


      if (J_UV == 0)
        break;                  /* ends loop */

      nenew = 0.5 * (ne + neold);
      ne = nenew;
      necgs = ne * nHcgs;


      if (fabs(ne - neold) < 1.0e-4)
        break;



      if (niter > (MAXITER - 10))
        printf("ne= %g  niter=%d\n", ne, niter);
    }
  while (niter < MAXITER);


  if (niter >= MAXITER)
    {
      printf("no convergence reached in ion_balance()\n");
      exit(0);
    }

}
