#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "global_vars.h"
#include "parameters.h"
#include "proto.h"

void
get_los_positions()
{

  double Hz, drbin, vmax, dvbin, rscale;

  int i;

  Hz = 100.0 * h100 * sqrt(omega0 / (atime * atime * atime) + omegaL);  /* km s^-1 Mpc^-1 */

  drbin = box100 / (double) NBINS;      /* comoving kpc/h */
  rscale = (KPC * atime) / h100;        /* convert length to cm */
  vmax = box100 * Hz * rscale / MPC;    /* box size */
  dvbin = vmax / (double) NBINS;        /* bin size (kms^-1) */

  for (i = 0; i < NBINS - 1; i++)
    {
      posaxis[i + 1] = posaxis[i] + drbin;      /* comoving kpc/h */
      velaxis[i + 1] = velaxis[i] + dvbin;      /* km s^-1 */
    }

#ifdef LOSTAB                   /* External ASCII file with line-of-sight positions */

  char fname[400];
  FILE *input;

  sprintf(fname, "%s", LOSFILE);
  if (!(input = fopen(fname, "r")))
    {
      printf("-DLOSTAB: loading external line-of-sight table\n");
      printf("Cannot read file %s\n\n", fname);
      exit(0);
    }
  printf("-DLOSTAB: loading external line-of-sight table\n");
  printf("Loaded %s\n\n", fname);

  for (i = 0; i < NUMLOS; i++)
    {
      int ierr = fscanf(input, "%d %lf %lf %lf", &ilos[i], &xlos[i], &ylos[i], &zlos[i]);
      if (ierr == EOF)
        {
          printf("ERROR: Unable to parse LOS file.");
        }
      xlos[i] *= 1.0e3;
      ylos[i] *= 1.0e3;
      zlos[i] *= 1.0e3;
    }

  fclose(input);

#else /* Random sight-lines or PRACE grid */

#ifdef PRACE_GRID

  /* Construct a regular grid for los coordinates.  This is the
   * format used in the PRACE on-the-fly extraction */

  int ngrid1, ngrid2, ngrid3, ngrid_tot, iproc, row, col;
  double dgrid1, dgrid2, dgrid3;

  ngrid1 = 50;
  ngrid2 = 40;
  ngrid3 = 30;

  ngrid_tot = ngrid1 * ngrid1 + ngrid2 * ngrid2 + ngrid3 * ngrid3;
  if (ngrid_tot != NUMLOS)
    {
      printf("\n\nError! When using PRACE_GRID, NUMLOS must be %d\n", ngrid_tot);
      printf("NUMLOS (see parameters.h) is currently %d\n\n", NUMLOS);
      exit(0);
    }

  dgrid1 = box100 / (double) ngrid1;
  dgrid2 = box100 / (double) ngrid2;
  dgrid3 = box100 / (double) ngrid3;

  /* project along x-axis */
  for (row = 0; row < ngrid1; row++)
    {
      for (col = 0; col < ngrid1; col++)
        {
          iproc = row * ngrid1 + col;
          ilos[iproc] = 1;
          xlos[iproc] = 0.5 * box100;
          ylos[iproc] = (col + 0.131213) * dgrid1;      /* Use random offset <0.5 to avoid ICs artifacts */
          zlos[iproc] = (row + 0.131213) * dgrid1;
        }
    }
  /* project along y-axis */
  for (row = 0; row < ngrid2; row++)
    {
      for (col = 0; col < ngrid2; col++)
        {
          iproc = ngrid1 * ngrid1 + row * ngrid2 + col;
          ilos[iproc] = 2;
          ylos[iproc] = 0.5 * box100;
          xlos[iproc] = (col + 0.241008) * dgrid2;
          zlos[iproc] = (row + 0.241008) * dgrid2;
        }
    }
  /* project along z-axis */
  for (row = 0; row < ngrid3; row++)
    {
      for (col = 0; col < ngrid3; col++)
        {
          iproc = ngrid1 * ngrid1 + ngrid2 * ngrid2 + row * ngrid3 + col;
          ilos[iproc] = 3;
          zlos[iproc] = 0.5 * box100;
          xlos[iproc] = (col + 0.170482) * dgrid3;
          ylos[iproc] = (row + 0.170482) * dgrid3;
        }
    }

#else /* Not PRACE_GRID */

  /* Random lines-of-sight */
  srand48(241008);

  for (i = 0; i < NUMLOS; i++)
    {
      do
        {
          ilos[i] = (int) (drand48() * 4);
        }
      while (ilos[i] == 0 || ilos[i] == 4);
      xlos[i] = drand48() * box100;
      ylos[i] = drand48() * box100;
      zlos[i] = drand48() * box100;
    }
#endif /* If PRACE_GRID ... else ... endif */

#endif /* If LOSTAB ... else ... endif */

}
