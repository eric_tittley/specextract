/* Make the look-up table for the collisional ionisation rates and
   recombination coefficients */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "proto.h"
#include "global_vars.h"

void
MakeCoolingTable(void)
{

  /* Verner & Ferland case-A recombination rate fit coefficients */
  double a_vf96[3] = { 7.982e-11, 9.356e-10, 1.891e-10 };
  double b_vf96[3] = { 0.7480, 0.7892, 0.7524 };
  double T0_vf96[3] = { 3.148e0, 4.266e-2, 9.370e0 };
  double T1_vf96[3] = { 7.036e5, 4.677e6, 2.7674e6 };

  /* Voronov collisional ionisation rate fit coefficients */
  double dE_vor97[3] = { 13.6, 24.6, 54.4 };
  double P_vor97[3] = { 0, 0, 1 };
  double A_vor97[3] = { 0.291e-7, 0.175e-7, 0.205e-8 };
  double X_vor97[3] = { 0.232, 0.180, 0.265 };
  double K_vor97[3] = { 0.39, 0.35, 0.25 };

  double T, T_eV;

  int i;

  logTmin = log10(TMIN);
  logTmax = log10(TMAX);
  deltaT = (logTmax - logTmin) / NCOOLTAB;

  for (i = 0; i <= NCOOLTAB; i++)
    {
      T = pow(10.0, logTmin + deltaT * i);
      T_eV = T * BOLTZMANN / ELECTRONVOLT;


      /* Case-A recombination rates, [cm^3 s^-1]
       * Verner & Ferland, 1996, ApJS, 103, 467 */
      AlphaHp[i] =
        a_vf96[0] / (sqrt(T / T0_vf96[0]) *
                     pow(1.0 + sqrt(T / T0_vf96[0]),
                         1.0 - b_vf96[0]) * pow(1.0 + sqrt(T / T1_vf96[0]), 1.0 + b_vf96[0]));

      AlphaHep[i] =
        a_vf96[1] / (sqrt(T / T0_vf96[1]) *
                     pow(1.0 + sqrt(T / T0_vf96[1]),
                         1.0 - b_vf96[1]) * pow(1.0 + sqrt(T / T1_vf96[1]), 1.0 + b_vf96[1]));

      AlphaHepp[i] =
        a_vf96[2] / (sqrt(T / T0_vf96[2]) *
                     pow(1.0 + sqrt(T / T0_vf96[2]),
                         1.0 - b_vf96[2]) * pow(1.0 + sqrt(T / T1_vf96[2]), 1.0 + b_vf96[2]));


      /* He+ dielectronic recombination rate [cm^3 s^-1]
       * Aldrovandi & Pequignot, 1973, A&A, 25, 137 */
      if (4.7e5 / T < 70)
        Alphad[i] = 1.9e-3 * (1.0 + 0.3 * exp(-9.4e4 / T)) * exp(-4.7e5 / T) * pow(T, -1.5);


      /* Collisional ionisation rates [cm^3 s^-1]
       * Voronov 1997, ADNDT, 65, 1 */
      if (dE_vor97[0] / T_eV < 70)
        GammaeH0[i] =
          A_vor97[0] * pow(dE_vor97[0] / T_eV, K_vor97[0])
          * exp(-dE_vor97[0] / T_eV)
          * (1.0 + P_vor97[0] * sqrt(dE_vor97[0] / T_eV)) / (X_vor97[0] + dE_vor97[0] / T_eV);

      if (dE_vor97[1] / T_eV < 70)
        GammaeHe0[i] =
          A_vor97[1] * pow(dE_vor97[1] / T_eV, K_vor97[1])
          * exp(-dE_vor97[1] / T_eV)
          * (1.0 + P_vor97[1] * sqrt(dE_vor97[1] / T_eV)) / (X_vor97[1] + dE_vor97[1] / T_eV);

      if (dE_vor97[2] / T_eV < 70)
        GammaeHep[i] =
          A_vor97[2] * pow(dE_vor97[2] / T_eV, K_vor97[2])
          * exp(-dE_vor97[2] / T_eV)
          * (1.0 + P_vor97[2] * sqrt(dE_vor97[2] / T_eV)) / (X_vor97[2] + dE_vor97[2] / T_eV);


    }

}
