
/* Function prototypes */

/* read_snapshot.c */
void allocate_particle_memory();
int load_snapshot_hdf5(int i);

/* extract_spectra.c */
void SPH_interpolation();
void InitLOSMemory();
void compute_absorption();
void normalise_fields();
void write_spectra();
void get_los_positions();

/* ion_balance.c */
#define SMALLNUM 1.0e-60
#define TMIN 1.0
#define TMAX 1.0e9
#define MAXITER  150
#define NCOOLTAB 2000
extern double gJH0, gJHe0, gJHep;

#ifdef SELF_SHIELD
extern double gJH0_SS;
#endif

extern double nHp, nHep, nHepp, nH0, nHe0, ne, necgs;
extern double aHp, aHep, aHepp, ad, geH0, geHe0, geHep;

extern double n0_z, alpha1_z, alpha2_z, beta_z, fval_z;

extern double deltaT;
extern double *AlphaHp, *AlphaHep, *AlphaHepp, *Alphad;
extern double *GammaeH0, *GammaeHe0, *GammaeHep;
extern double logTmin, logTmax;

#define TABLESIZE 500

extern float inlogz[TABLESIZE];
extern float gH0[TABLESIZE], gHe[TABLESIZE], gHep[TABLESIZE];
extern float eH0[TABLESIZE], eHe[TABLESIZE], eHep[TABLESIZE];
extern int nheattab;            /* length of table */

double self_shield(double nHcgs, double logT, double ne_input);
void ion_balance(double nHcgs, double logT, double ne_input);
void InitCool(void);
void ReadIonizeParams(char *fname);
void IonizeParamsTable();
void MakeCoolingTable(void);
void InitCoolMemory(void);
void SelfShieldFit();

/* Globals */
extern double *rhoker_H,*posaxis,*velaxis;
extern double *rhoker_H1,*velker_H1,*tempker_H1,*tau_H1;
#ifdef METALS
extern double *metker;
#endif
extern double *xlos,*ylos,*zlos;
extern int *ilos;

extern double vmax,vmax2,drbin;

extern char *path,*snapbase;
extern int  snapshot;
