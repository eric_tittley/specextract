#include <stdio.h>

/*---------------------- Global parameters ---------------------------*/

/* Snapshot information */
#define FILENUMBER 4 /* Number of snapshot sub-files */

/* Spectra information */
#define NBINS  1024 /* number of pixels in each line-of-sight*/
#define NUMLOS 100  /* number of lines-of-sight */

/* The UV background file (should be placed directory with executable) */
#define UVBFILE "TREECOOL_HM12_G+Q"

/* --------------------- OPTS +=-DLOSTAB -----------------------------*/

/* Path and name for external table with LOS co-ordinates */
/* The table format is: axis(=1,2 or 3), x, y, z [cMpc/h] */

#define LOSFILE  "table_testhalo.txt"

/*--------------------------------------------------------------------*/
