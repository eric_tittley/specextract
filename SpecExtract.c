#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <hdf5.h>


#include "global_vars.h"
#include "parameters.h"
#include "proto.h"

int
main(int argc, char **argv)
{
  int i;

  printf("\n");

  if (argc != 3)
    {
      fprintf(stderr, "\n\nIncorrect argument(s).  Specify:\n\n");
      fprintf(stderr, "<path>     (path to output files)\n");
      fprintf(stderr, "<num>      (number of snapshot)\n\n");
      fprintf(stderr, "Usage: ./SpecExtract <path> <num>\n");
      fprintf(stderr, "\n\n");
      exit(0);
    }

  path = argv[1];
  snapshot = atoi(argv[2]);

#if defined(LOSGRID) && defined(LOSTAB)
  printf("\n\nThe flags LOSGRID and LOSTAB cannot be raised simultaenously\n");
  printf("Adjust the parameter settings in Makefile\n\n");
  exit(0);
#endif

#if defined(SELF_SHIELD) && !(defined(IONBALANCE))
  printf("\n\nThe flag SELF_SHIELD requires IONBALANCE to be raised as well\n");
  printf("Adjust the parameter settings in Makefile\n\n");
  exit(0);
#endif

#if defined(NOCOLLISIONAL) && !(defined(IONBALANCE))
  printf("\n\nThe flag NOCOLLISIONAL requires IONBALANCE to be raised as well\n");
  printf("Adjust the parameter settings in Makefile\n\n");
  exit(0);
#endif

  /* Load multiple files and project */
  for (i = 0; i < FILENUMBER; i++)
    {
      load_snapshot_hdf5(i);

      if (i == 0)
        {
          InitLOSMemory();

          get_los_positions();

#ifdef IONBALANCE
          InitCool();
#endif

          printf("Assuming hydrogen fraction by mass of %f\n\n", XH);
        }

      printf("Extracting LOS from sub-file %d...\n", i);
      SPH_interpolation();
      printf("Done.\n\n");

      free(P);
    }

  printf("Omegab0 = %f\n\n", omegab);

  printf("Normalising...\n");
  normalise_fields();
  printf("Done.\n\n");

  printf("Computing optical depths...\n");
  compute_absorption();
  printf("Done.\n\n");

  printf("Saving spectra...\n");
  write_spectra();
  printf("Done.\n\n");

  exit(0);
}
