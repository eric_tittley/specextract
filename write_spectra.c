#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "global_vars.h"
#include "parameters.h"
#include "proto.h"

void
write_spectra()
{
  double ztime[1], om_out[1], ol_out[1], ob_out[1], h_out[1], box_out[1], xh_out[1];
  int nbins_spec[1], nlos_spec[1];

  char fname[400];

  FILE *output;


  ztime[0] = 1.0 / atime - 1.0;
  om_out[0] = omega0;
  ol_out[0] = omegaL;
  ob_out[0] = omegab;
  h_out[0] = h100;
  xh_out[0] = XH;
  box_out[0] = box100;

  nbins_spec[0] = (int) NBINS;
  nlos_spec[0] = (int) NUMLOS;

  /* Saves the output in the format used for the on-the-fly data, so
   * that the LOS and optical depth data is split over two files  */
#ifdef LOSEXTRACT

  sprintf(fname, "los%d_n%d_z%.3f.dat", NBINS, NUMLOS, ztime[0]);
  if (!(output = fopen(fname, "wb")))
    {
      printf("\nCannot write file:\n");
      printf("%s\n", fname);
      exit(0);
    }

  /* Header */
  fwrite(ztime, sizeof(double), 1, output);
  fwrite(om_out, sizeof(double), 1, output);
  fwrite(ol_out, sizeof(double), 1, output);
  fwrite(ob_out, sizeof(double), 1, output);
  fwrite(h_out, sizeof(double), 1, output);
  fwrite(box_out, sizeof(double), 1, output);
  fwrite(xh_out, sizeof(double), 1, output);
  fwrite(nbins_spec, sizeof(int), 1, output);
  fwrite(nlos_spec, sizeof(int), 1, output);

  /* Positions */
  fwrite(ilos, sizeof(int), NUMLOS, output);
  fwrite(xlos, sizeof(double), NUMLOS, output);
  fwrite(ylos, sizeof(double), NUMLOS, output);
  fwrite(zlos, sizeof(double), NUMLOS, output);
  fwrite(posaxis, sizeof(double), NBINS, output);       /* pixel positions, comoving kpc/h */
  fwrite(velaxis, sizeof(double), NBINS, output);       /* pixel positions, km s^-1 */

  fwrite(rhoker_H, sizeof(double), NBINS * NUMLOS, output);     /* gas overdensity  */

  /* HI Lyman-alpha */
  fwrite(rhoker_H1, sizeof(double), NBINS * NUMLOS, output);    /* n_HI/n_H */
  fwrite(tempker_H1, sizeof(double), NBINS * NUMLOS, output);   /* T [K], HI weighted */
  fwrite(velker_H1, sizeof(double), NBINS * NUMLOS, output);    /* v_pec [km s^-1], HI weighted */

#ifdef METALS
  /* Metals */
  fwrite(metker, sizeof(double), NBINS * NUMLOS, output);       /* Metallicity */
#endif

  fclose(output);

  sprintf(fname, "tau%d_n%d_z%.3f.dat", NBINS, NUMLOS, ztime[0]);
  if (!(output = fopen(fname, "wb")))
    {
      printf("\nCannot write file:\n");
      printf("%s\n", fname);
      exit(0);
    }
  fwrite(tau_H1, sizeof(double), NBINS * NUMLOS, output);
  fclose(output);


#else


  sprintf(fname, "spec%d_n%d_z%.3f.dat", NBINS, NUMLOS, ztime[0]);
  if (!(output = fopen(fname, "wb")))
    {
      printf("\nCannot write file:\n");
      printf("%s\n", fname);
      exit(0);
    }

  /* Header */
  fwrite(ztime, sizeof(double), 1, output);
  fwrite(om_out, sizeof(double), 1, output);
  fwrite(ol_out, sizeof(double), 1, output);
  fwrite(ob_out, sizeof(double), 1, output);
  fwrite(h_out, sizeof(double), 1, output);
  fwrite(box_out, sizeof(double), 1, output);
  fwrite(xh_out, sizeof(double), 1, output);
  fwrite(nbins_spec, sizeof(int), 1, output);
  fwrite(nlos_spec, sizeof(int), 1, output);

  /* Positions */
  fwrite(ilos, sizeof(int), NUMLOS, output);
  fwrite(xlos, sizeof(double), NUMLOS, output);
  fwrite(ylos, sizeof(double), NUMLOS, output);
  fwrite(zlos, sizeof(double), NUMLOS, output);
  fwrite(posaxis, sizeof(double), NBINS, output);       /* pixel positions, comoving kpc/h */
  fwrite(velaxis, sizeof(double), NBINS, output);       /* pixel positions, km s^-1 */

  fwrite(rhoker_H, sizeof(double), NBINS * NUMLOS, output);     /* gas overdensity  */

  /* HI Lyman-alpha */
  fwrite(rhoker_H1, sizeof(double), NBINS * NUMLOS, output);    /* n_HI/n_H */
  fwrite(tempker_H1, sizeof(double), NBINS * NUMLOS, output);   /* T [K], HI weighted */
  fwrite(velker_H1, sizeof(double), NBINS * NUMLOS, output);    /* v_pec [km s^-1], HI weighted */
  fwrite(tau_H1, sizeof(double), NBINS * NUMLOS, output);       /* HI optical depth */

#ifdef METALS
  fwrite(metker, sizeof(double), NBINS * NUMLOS, output);       /* Absolute gas metallicity */
#endif

  fclose(output);

#endif
}
