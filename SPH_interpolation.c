#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "global_vars.h"
#include "parameters.h"
#include "proto.h"

/* Perform the SPH interpolation along the sight-lines */
void
SPH_interpolation()
{

#ifndef _OPENMP
  double pcdone, pccount = 0.0;
#endif

  double const Hz = 100.0 * h100 * sqrt(omega0 / (atime * atime * atime) + omegaL);     /* km s^-1 Mpc^-1 */

  /* Conversion factors from internal units */
  double const rscale = (KPC * atime) / h100;   /* convert length to cm */
  double const vscale = sqrt(atime);    /* convert velocity to kms^-1 */
  double const mscale = (1.0e10 * SOLAR_MASS) / h100;   /* convert mass to g */
  double const escale = 1.0e10; /* convert energy/unit mass from(km/s)^2 to (cm/s)^2 */
  double const dscale = mscale / (rscale * rscale * rscale);    /* convert density to g cm^-3 */

  /* Box scales in internal units */
  drbin = box100 / (double) NBINS;      /* bin size */
  double const drinv = 1.0 / drbin;
  double const box2 = 0.5 * box100;

  /* Box scales in km s^-1 */
  vmax = box100 * Hz * rscale / MPC;    /* box size */
  vmax2 = 0.5 * vmax;


  size_t i;
#pragma omp parallel for default(none) shared(NumPart_ThisFile,P) private(i)
  for (i = 0; i < NumPart_ThisFile[0]; i++)
    {
      double const mu = 1.0 / (XH * (0.75 + P[i].Ne) + 0.25);   /* Mean molecular weight */
      P[i].U *= ((GAMMA - 1.0) * mu * HMASS * AMU) / BOLTZMANN; /* K/escale */

#ifdef IONBALANCE
      if (J_UV != 0)
        P[i].NH0 = self_shield(XH * dscale * P[i].Rho / PROTONMASS, log10(P[i].U * escale), P[i].Ne);
#endif
    }


  /* SPH interpolation onto sight-lines */
  size_t iproc;
#pragma omp parallel for default(none) \
         shared(NumPart_ThisFile,P,ilos,xlos,ylos,zlos,box100,drbin,rhoker_H,rhoker_H1,velker_H1,tempker_H1,metker) \
         private(iproc,i) \
         schedule(dynamic)
  for (iproc = 0; iproc < NUMLOS; iproc++)
    {
      for (i = 0; i < NumPart_ThisFile[0]; i++)
        {
          double const xx = P[i].Pos[0];
          double const yy = P[i].Pos[1];
          double const zz = P[i].Pos[2];

          double const hh = P[i].h;

          /* distance to projection axis */

          double dr = (ilos[iproc] == 1) ? fabs(yy - ylos[iproc]) : fabs(xx - xlos[iproc]);

          if (dr > box2)
            dr = box100 - dr;

          /* particle is within h of the axis */
          if (dr <= hh)
            {
              double dr2 = dr * dr;

              /* distance to projection axis */
              dr = (ilos[iproc] == 3) ? fabs(yy - ylos[iproc]) : fabs(zz - zlos[iproc]);

              if (dr > box2)
                dr = box100 - dr;

              dr2 += dr * dr;   /* dx^2+dy^2 */

              double const h2 = hh * hh;

              /* particle is within h of the axis */
              if (dr2 <= h2)
                {
                  /* Central pixel to contribute to */
                  long int iz = 0;
                  if (ilos[iproc] == 1)
                    {
                      iz = xx * drinv + 0.5;
                    }
                  else if (ilos[iproc] == 2)
                    {
                      iz = yy * drinv + 0.5;
                    }
                  else if (ilos[iproc] == 3)
                    {
                      iz = zz * drinv + 0.5;
                    }

                  /* max distance along line-of-sight axis from central
                   * pixel lying within h of contributing particle */
                  double const drmax = sqrt(fabs(h2 - dr2));
                  /* The following is a dangerous conversion from double to int
                   * without a conversion rule, like floor() or ceil() */
                  long int const ioff = (long int) (drmax * drinv);

                  /* Loop over contributing pixels */
                  long int iiz;
                  for (iiz = iz - ioff; iiz < iz + ioff + 1; iiz++)
                    {
                      /* maintain periodic indices */
                      long int j = iiz;
                      if (iiz < 0)
                        j = NBINS + iiz;
                      if (iiz > NBINS - 1)
                        j = iiz - NBINS;
                      size_t pixel_index = (size_t) j + NBINS * iproc;

                      double const rgrid = (double) (j) * drbin;

                      if (ilos[iproc] == 1)
                        {
                          dr = fabs(rgrid - xx);
                        }
                      else if (ilos[iproc] == 2)
                        {
                          dr = fabs(rgrid - yy);
                        }
                      else if (ilos[iproc] == 3)
                        {
                          dr = fabs(rgrid - zz);
                        }

                      if (dr > box2)
                        dr = box100 - dr;

                      double const dist2 = dr2 + dr * dr;       /* dx^2+dy^2+dz^2 */

                      if (dist2 <= h2)
                        {
                          double const hinv2 = 1.0 / h2;        /* 1/h^2 */
                          double const q = sqrt(dist2 * hinv2); /* q=r/h */
                          double const mq = 1.0 - q;


                          /* Cubic spline kernel used in P-Gadget-3.
                           * See Eq. (4) in Springel et al. 2005, MNRAS,
                           * 364, 1105 */
                          double wk = (q <= 0.5) ? 2.546479089 - 15.27887454 * q * q * mq : 5.092958179 * mq * mq * mq;

                          wk *= dscale * P[i].Mass * XH * hinv2 / hh;   /* g cm^-3 */

                          rhoker_H[pixel_index] += wk;  /* g cm^-3 */
                          rhoker_H1[pixel_index] += wk * P[i].NH0;      /* g cm^-3 */
                          tempker_H1[pixel_index] += wk * P[i].U * escale * P[i].NH0;   /* g cm^-3 * K */
                          velker_H1[pixel_index] += wk * P[i].Vel[ilos[iproc] - 1] * vscale * P[i].NH0; /* g cm^-3 * km s^-1 */
#ifdef METALS
                          metker[pixel_index] += wk * P[i].Z;   /* g cm^-3 */
#endif
                        }       /* r < h */
                    }           /* contributing vertices */
                }               /* dx^2+dy^2 < h^2 */
            }                   /* dr < h */
        }                       /* particles in LOS */


#ifndef _OPENMP
      pcdone = 100.0 * (float) iproc / (float) NUMLOS;
      if (pcdone >= pccount)
        {
          printf("%3.2f%%\n", pcdone);
          pccount += 10.0;
        }
#endif

    }                           /* iproc < NUMLOS */

  return;
}
