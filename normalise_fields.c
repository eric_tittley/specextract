
#include "global_vars.h"
#include "parameters.h"
#include "proto.h"

void
normalise_fields()
{

  double const H0 = 1.0e7 / MPC;        /* 100 km s^-1 Mpc^-1 in cgs */
  double const rhoc = 3.0 * (H0 * h100) * (H0 * h100) / (8.0 * PI * GRAVITY);   /* g cm^-3 */
  double const critH = rhoc * omegab * XH / (atime * atime * atime);    /* g cm^-3 */

  /* Normalise quantities */
  size_t iproc;
#pragma omp parallel for default(none) \
         shared(velker_H1,tempker_H1,rhoker_H1,rhoker_H,metker) \
         private(iproc) \
         schedule(static)
  for (iproc = 0; iproc < NUMLOS; iproc++)
    {
      size_t i;
      for (i = 0; i < NBINS; i++)
        {
          size_t const pixel_index = i + NBINS * iproc;

          velker_H1[pixel_index] /= rhoker_H1[pixel_index];     /* HI weighted km s^-1 */
          tempker_H1[pixel_index] /= rhoker_H1[pixel_index];    /* HI weighted K */
          rhoker_H1[pixel_index] /= rhoker_H[pixel_index];      /* HI/H */
#ifdef METALS
          metker[pixel_index] /= rhoker_H[pixel_index]; /* Metallicity */
#endif
          rhoker_H[pixel_index] /= critH;
        }
    }
}
