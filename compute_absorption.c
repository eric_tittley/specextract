#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "global_vars.h"
#include "parameters.h"
#include "proto.h"

void
compute_absorption()
{

#ifndef _OPENMP
  double pcdone, pccount = 0.0;
#endif

  double const rscale = (KPC * atime) / h100;   /* comoving kpc/h to cm */
  double const escale = 1.0e10; /* (km s^-1)^2 to (cm s^-1)^2 */
  double const drbin = box100 / (double) NBINS; /* comoving kpc/h */
  double const H0 = 1.0e7 / MPC;        /* 100 km s^-1 Mpc^-1 in cgs */
  double const rhoc = 3.0 * (H0 * h100) * (H0 * h100) / (8.0 * PI * GRAVITY);   /* g cm^-3 */
  double const critH = rhoc * omegab * XH / (atime * atime * atime);    /* g cm^-3 */

  /* HI Lyman-alpha */
  double const sigma_Lya_H1 = sqrt(3.0 * PI * SIGMA_T / 8.0) * LAMBDA_LYA_H1 * FOSC_LYA;        /* cm^2 */
  double const k1_H1 = 2.0 * BOLTZMANN / (HMASS * AMU);
  double const k2_H1 = GAMMA_LYA_H1 * LAMBDA_LYA_H1 / (4.0 * PI);
  double const k3_H1 = sigma_Lya_H1 * C * rscale * drbin * critH / (sqrt(PI) * HMASS * AMU);

  size_t iproc;
#pragma omp parallel for default(none) \
         shared(velaxis,velker_H1,tempker_H1,rhoker_H,rhoker_H1,vmax2,vmax,tau_H1) \
         private(iproc) \
         schedule(static)
  for (iproc = 0; iproc < NUMLOS; iproc++)
    {
      double u_H1[NBINS];
      double b2inv_H1[NBINS];
      double aa_H1[NBINS];
      double tau_delta_H1[NBINS];

      size_t j;
      for (j = 0; j < NBINS; j++)
        {
          size_t convol_index = j + NBINS * iproc;

          /* HI Lyman-alpha */
          double const binv_H1 = 1.0 / sqrt(k1_H1 * tempker_H1[convol_index]);  /* cm s^-1 */
          u_H1[j] = velaxis[j] + velker_H1[convol_index];       /* km s^-1 */
          b2inv_H1[j] = escale * binv_H1 * binv_H1;     /* (km s^-1)^-2 */
          aa_H1[j] = k2_H1 * binv_H1;
          tau_delta_H1[j] = k3_H1 * rhoker_H[convol_index] * rhoker_H1[convol_index] * binv_H1;
        }

      /* Voigt profile: Tepper-Garcia, 2006, MNRAS, 369, 2025 */
      size_t i;
      for (i = 0; i < NBINS; i++)
        {
          size_t pixel_index = i + NBINS * iproc;

          for (j = 0; j < NBINS; j++)
            {
              /* HI Lyman-alpha */
              double vdiff_H1 = fabs(velaxis[i] - u_H1[j]);
              if (vdiff_H1 > vmax2)
                vdiff_H1 = vmax - vdiff_H1;

              double const T0 = vdiff_H1 * vdiff_H1 * b2inv_H1[j];
              double const T1 = exp(-T0);
              double const T2 = 1.5 / T0;

              double const profile_H1 =
                (T0 <
                 1.0e-6) ? T1 : T1 - aa_H1[j] / sqrt(PI) / T0 * (T1 * T1 * (4.0 * T0 * T0 + 7.0 * T0 + 4.0 + T2) - T2 -
                                                                 1.0);

              tau_H1[pixel_index] += tau_delta_H1[j] * profile_H1;
            }
        }


#ifndef _OPENMP
      pcdone = 100.0 * (double) iproc / (double) NUMLOS;
      if (pcdone >= pccount)
        {
          printf("%3.2f%%\n", pcdone);
          pccount += 10.0;
        }
#endif

    }
}
