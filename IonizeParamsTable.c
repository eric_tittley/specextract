/* Computes the photo-ionisation and photo-heating rates from the
   external TREECOOL file*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "proto.h"
#include "global_vars.h"

void
IonizeParamsTable()
{
  int i, ilow;
  float logz, dzlow, dzhi;


  logz = log10(redshift + 1.0);
  ilow = 0;

  for (i = 0; i < nheattab; i++)
    {
      if (inlogz[i] < logz)
        ilow = i;
      else
        break;
    }

  dzlow = logz - inlogz[ilow];
  dzhi = inlogz[ilow + 1] - logz;

  if (logz > inlogz[nheattab - 1] || gH0[ilow] == 0 || gH0[ilow + 1] == 0 || nheattab == 0)
    {
      gJHe0 = gJHep = gJH0 = 0.0;
      J_UV = 0.0;
      return;
    }
  else
    J_UV = 1.e-21;              /* irrelevant as long as it's not 0 */


  gJH0 = pow(10., (dzhi * log10(gH0[ilow]) + dzlow * log10(gH0[ilow + 1])) / (dzlow + dzhi));
  gJHe0 = pow(10., (dzhi * log10(gHe[ilow]) + dzlow * log10(gHe[ilow + 1])) / (dzlow + dzhi));
  gJHep = pow(10., (dzhi * log10(gHep[ilow]) + dzlow * log10(gHep[ilow + 1])) / (dzlow + dzhi));

  return;
}
