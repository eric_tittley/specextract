
#######################################################################
#  Look at end of file for a brief guide to any compile-time options. #
#######################################################################

#--------------------------------------- Spectra extraction options

#OPTS +=-DLOSEXTRACT	
#OPTS +=-DPRACE_GRID	
#OPTS +=-DLOSTAB     
OPTS +=-DMETALS

#--------------------------------------- Select system
#SYSTYPE="Darwin"
#SYSTYPE="pppzjsb"
#SYSTYPE="dator"
#SYSTYPE="brahan"


ifeq ($(SYSTYPE),"Darwin")
CC = icc
OPTIMIZE = -O3
CFLAGS =  $(OPTIMIZE) -Wall -xHOST -ip -ipo
HDF5INCL = -I/usr/local/Cluster-Apps/hdf5/1.8.0/include/
HDF5LIB  = -L/usr/local/Cluster-Apps/hdf5/1.8.0/lib/ -lhdf5 -lz
endif

ifeq ($(SYSTYPE),"pppzjsb")
CC = gcc		
OPTIMIZE = -O3
CFLAGS = $(OPTIMIZE) -Wall
HDF5INCL = -I/home/ppzjsb/FILES/Libraries/hdf5-1.8.11-linux-x86_64-shared/include
HDF5LIB  = -L/home/ppzjsb/FILES/Libraries/hdf5-1.8.11-linux-x86_64-shared/lib/ -lhdf5 -lz
endif

ifeq ($(SYSTYPE),"dator")
CC = gcc		
OPTIMIZE = -O3
CFLAGS = $(OPTIMIZE) -Wall
HDF5INCL = -I/home/ppzjsb/Libraries/hdf5-1.8.11-linux-x86_64-shared/include
HDF5LIB  = -L/home/ppzjsb/Libraries/hdf5-1.8.11-linux-x86_64-shared/lib/ -lhdf5 -lsz 
endif

ifeq ($(SYSTYPE),"brahan")
CC = gcc		
OPTIMIZE = -O3
CFLAGS = $(OPTIMIZE) -Wall
HDF5INCL = -I/home/ppzjsb/Libraries/hdf5-1.8.11-linux-x86_64-shared/include
HDF5LIB  = -L/home/ppzjsb/Libraries/hdf5-1.8.11-linux-x86_64-shared/lib/ -lhdf5 -lsz 
endif

ifeq ($(HOST),arrakis)
 CC = gcc
 CFLAGS_OPTIMIZE = -O3 -fopenmp -flto
 #CFLAGS_PROFILE = -O1 -pg
 #CFLAGS_DEBUG = -O0 -g -Wall
 CFLAGS =  $(CFLAGS_OPTIMIZE) $(CFLAGS_PROFILE) $(CFLAGS_DEBUG)
 HDF5INCL = -I/usr/include/hdf5/serial
 HDF5LIB  = -lhdf5_serial
endif

ifeq ($(HOST),cuillin)
 CC = gcc
 CFLAGS_OPTIMIZE = -O3 -fopenmp -flto
 #CFLAGS_PROFILE = -O1 -pg
 #CFLAGS_DEBUG = -O0 -g -Wall
 CFLAGS =  $(CFLAGS_OPTIMIZE) $(CFLAGS_PROFILE) $(CFLAGS_DEBUG)
 HDF5INCL = -I/usr/include/hdf5/serial
 HDF5LIB  = -lhdf5_serial
endif

CFLAGS += $(OPTS)

EXEC = SpecExtract

SRC_C = SpecExtract.c \
        allocate_particle_memory.c \
        compute_absorption.c \
        get_los_positions.c \
        globals.c \
        InitLOSMemory.c \
        load_snapshot_hdf5.c \
        normalise_fields.c \
        SPH_interpolation.c \
        write_spectra.c \
        InitCool.c \
        InitCoolMemory.c \
        self_shield.c \
        MakeCoolingTable.c \
        ReadIonizeParams.c \
        IonizeParamsTable.c \
        SelfShieldFit.c \
        ion_balance.c

OBJS = $(SRC_C:.c=.o)

INCL = parameters.h global_vars.h proto.h 

LIBS = -lm $(HDF5LIB)

CPPFLAGS +=  $(HDF5INCL)

$(EXEC): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) $(LIBS) -o $(EXEC)

$(OBJS): $(INCL)

.PHONY: clean distclean indent
clean:
	-rm -f $(OBJS)
	-rm -f *~

distclean: clean
	-rm -f $(EXEC) 

indent:
	indent $(SRC_C)
##############################################################################
#
#
# Spectra extraction options:
#
#	- LOSEXTRACT 	Saves the output in the format used for the on-the-fly
#			data, so that the LOS and optical depth data is split 
#			over two files 
#
#	- PRACE_GRID	Extract 5000 sight-lines on a regular grid, 50^2, 40^2,
#			30^2 along each axis. These are the positions used in 
#			the PRACE on-the-fly extraction 
#
#	- LOSTAB	pre-define line-of-sight positions in an external 
# 			table (see parameters.h)	
#
#
##############################################################################
