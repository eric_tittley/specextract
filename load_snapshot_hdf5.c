/* loads the particle data from HDF5 snapshot files */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <hdf5.h>


#include "global_vars.h"
#include "parameters.h"
#include "proto.h"

/* loads the particle data from HDF5 snapshot files */
int
load_snapshot_hdf5(int i)
{
  FILE *fd;
  char buf[400];
  int k, n;
  hid_t hdf5_file, hdf5_grp, hdf5_attribute, hdf5_dataset, dapl_id = 0;
  float *dummy3_float, *dummy_float;

  static double fgas = 0.0, fdark = 0.0, fstars = 0.0, fbhs = 0.0, ftot = 0.0;


  if (FILENUMBER > 1)
    sprintf(buf, "%s/snapdir_%03d/snap_%03d.%d.hdf5", path, snapshot, snapshot, i);
  else
    sprintf(buf, "%s/snap_%03d.hdf5", path, snapshot);

  if (!(fd = fopen(buf, "r")))
    {
      printf("can't open file `%s`\n\n", buf);
      exit(0);
    }

  /* Open HDF5 file */
  hdf5_file = H5Fopen(buf, H5F_ACC_RDONLY, H5P_DEFAULT);

  /* Read the header data */
  hdf5_grp = H5Gopen(hdf5_file, "/Header", H5P_DEFAULT);

  hdf5_attribute = H5Aopen_name(hdf5_grp, "NumPart_ThisFile");
  H5Aread(hdf5_attribute, H5T_NATIVE_INT, NumPart_ThisFile);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_grp, "NumPart_Total");
  H5Aread(hdf5_attribute, H5T_NATIVE_UINT, NumPart_Total_LowWord);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_grp, "NumPart_Total_HighWord");
  H5Aread(hdf5_attribute, H5T_NATIVE_UINT, NumPart_Total_HighWord);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_grp, "MassTable");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, mass);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_grp, "Time");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &atime);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_grp, "Redshift");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &redshift);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_grp, "BoxSize");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &box100);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_grp, "Omega0");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &omega0);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_grp, "OmegaLambda");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &omegaL);
  H5Aclose(hdf5_attribute);

  hdf5_attribute = H5Aopen_name(hdf5_grp, "HubbleParam");
  H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &h100);
  H5Aclose(hdf5_attribute);

  H5Gclose(hdf5_grp);


  for (k = 0; k < 6; k++)
    NumPart_Total[k] = (long long) NumPart_Total_LowWord[k] + ((long long) NumPart_Total_HighWord[k] << 32);

  if (i == 0)
    {
      for (k = 0; k < 6; k++)
        {
          printf("NumPart_ThisFile[%d] = %d\n", k, NumPart_ThisFile[k]);
          printf("NumPart_Total[%d]    = %lli\n", k, NumPart_Total[k]);
          printf("Mass[%d]             = %e\n\n", k, mass[k]);
        }

      printf("Redshift = %f\n", redshift);
      printf("a(t)     = %f\n", atime);
      printf("Lambda   = %f\n", omegaL);
      printf("Matter   = %f\n", omega0);
      printf("Hubble   = %f\n", h100);
      printf("Box      = %f\n\n", box100);
    }


  /* Allocate memory for particles in this sub-file */
  allocate_particle_memory();

  /* Gas particles */
  hdf5_grp = H5Gopen(hdf5_file, "/PartType0", H5P_DEFAULT);

  /* Positions, ckpc/h */
  dummy3_float = (float *) calloc(NumPart_ThisFile[0] * 3, sizeof(float));
  if (NULL == dummy3_float)
    {
      free(dummy3_float);
      printf("Memory allocation failed in read_snapshot.c\n");
      exit(0);
    }
  hdf5_dataset = H5Dopen(hdf5_grp, "Coordinates", dapl_id);
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, dummy3_float);
  H5Dclose(hdf5_dataset);
  for (n = 0; n < NumPart_ThisFile[0]; n++)
    {
      P[n].Pos[0] = dummy3_float[3 * n];
      P[n].Pos[1] = dummy3_float[3 * n + 1];
      P[n].Pos[2] = dummy3_float[3 * n + 2];
    }
  free(dummy3_float);
  if (i == 0)
    {
      printf("P[0].Pos[0] = %f\n", P[0].Pos[0]);
      printf("P[0].Pos[1] = %f\n", P[0].Pos[1]);
      printf("P[0].Pos[2] = %f\n", P[0].Pos[2]);
    }


  /* Velocities, sqrt(a) km/s */
  dummy3_float = (float *) calloc(NumPart_ThisFile[0] * 3, sizeof(float));
  if (NULL == dummy3_float)
    {
      free(dummy3_float);
      printf("Memory allocation failed in read_snapshot.c\n");
      exit(0);
    }
  hdf5_dataset = H5Dopen(hdf5_grp, "Velocities", dapl_id);
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, dummy3_float);
  H5Dclose(hdf5_dataset);
  for (n = 0; n < NumPart_ThisFile[0]; n++)
    {
      P[n].Vel[0] = dummy3_float[3 * n];
      P[n].Vel[1] = dummy3_float[3 * n + 1];
      P[n].Vel[2] = dummy3_float[3 * n + 2];
    }
  free(dummy3_float);
  if (i == 0)
    {
      printf("P[0].Vel[0] = %f\n", P[0].Vel[0]);
      printf("P[0].Vel[1] = %f\n", P[0].Vel[1]);
      printf("P[0].Vel[2] = %f\n", P[0].Vel[2]);
    }


  /* Masses, 10^10 Msol/h */
  dummy_float = (float *) calloc(NumPart_ThisFile[0], sizeof(float));
  if (NULL == dummy_float)
    {
      free(dummy_float);
      printf("Memory allocation failed in read_snapshot.c\n");
      exit(0);
    }
  hdf5_dataset = H5Dopen(hdf5_grp, "Masses", dapl_id);
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, dummy_float);
  H5Dclose(hdf5_dataset);
  for (n = 0; n < NumPart_ThisFile[0]; n++)
    {
      P[n].Mass = dummy_float[n];
      fgas += dummy_float[n];
    }
  free(dummy_float);
  if (i == 0)
    printf("P[0].Mass   = %e\n", P[0].Mass);


  /* Internal energy, (km/s)^2 */
  dummy_float = (float *) calloc(NumPart_ThisFile[0], sizeof(float));
  if (NULL == dummy_float)
    {
      free(dummy_float);
      printf("Memory allocation failed in read_snapshot.c\n");
      exit(0);
    }
  hdf5_dataset = H5Dopen(hdf5_grp, "InternalEnergy", dapl_id);
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, dummy_float);
  H5Dclose(hdf5_dataset);
  for (n = 0; n < NumPart_ThisFile[0]; n++)
    P[n].U = dummy_float[n];
  if (i == 0)
    printf("P[0].U      = %f\n", P[0].U);


  /* Density, 10^10 Msol/h (ckpc/h)^-3 */
  dummy_float = (float *) calloc(NumPart_ThisFile[0], sizeof(float));
  if (NULL == dummy_float)
    {
      free(dummy_float);
      printf("Memory allocation failed in read_snapshot.c\n");
      exit(0);
    }
  hdf5_dataset = H5Dopen(hdf5_grp, "Density", dapl_id);
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, dummy_float);
  H5Dclose(hdf5_dataset);
  for (n = 0; n < NumPart_ThisFile[0]; n++)
    P[n].Rho = dummy_float[n];
  free(dummy_float);
  if (i == 0)
    printf("P[0].Rho    = %e\n", P[0].Rho);


  /* ne/nH */
  dummy_float = (float *) calloc(NumPart_ThisFile[0], sizeof(float));
  if (NULL == dummy_float)
    {
      free(dummy_float);
      printf("Memory allocation failed in read_snapshot.c\n");
      exit(0);
    }
  hdf5_dataset = H5Dopen(hdf5_grp, "ElectronAbundance", dapl_id);
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, dummy_float);
  H5Dclose(hdf5_dataset);
  for (n = 0; n < NumPart_ThisFile[0]; n++)
    P[n].Ne = dummy_float[n];
  free(dummy_float);
  if (i == 0)
    printf("P[0].Ne     = %e\n", P[0].Ne);


  /* nH1/nH */
  dummy_float = (float *) calloc(NumPart_ThisFile[0], sizeof(float));
  if (NULL == dummy_float)
    {
      free(dummy_float);
      printf("Memory allocation failed in read_snapshot.c\n");
      exit(0);
    }
  hdf5_dataset = H5Dopen(hdf5_grp, "NeutralHydrogenAbundance", dapl_id);
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, dummy_float);
  H5Dclose(hdf5_dataset);
  for (n = 0; n < NumPart_ThisFile[0]; n++)
    P[n].NH0 = dummy_float[n];
  free(dummy_float);
  if (i == 0)
    printf("P[0].NH0    = %e\n", P[0].NH0);

#ifdef METALS
  /* Absolute gas metallicity */
  dummy_float = (float *) calloc(NumPart_ThisFile[0], sizeof(float));
  if (NULL == dummy_float)
    {
      free(dummy_float);
      printf("Memory allocation failed in read_snapshot.c\n");
      exit(0);
    }
  hdf5_dataset = H5Dopen(hdf5_grp, "Metallicity", dapl_id);
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, dummy_float);
  H5Dclose(hdf5_dataset);
  for (n = 0; n < NumPart_ThisFile[0]; n++)
    P[n].Z = dummy_float[n];
  free(dummy_float);
  if (i == 0)
    printf("P[0].Z      = %e\n", P[0].Z);
#endif


  /* Smoothing length, ckpc/h */
  dummy_float = (float *) calloc(NumPart_ThisFile[0], sizeof(float));
  if (NULL == dummy_float)
    {
      free(dummy_float);
      printf("Memory allocation failed in read_snapshot.c\n");
      exit(0);
    }
  hdf5_dataset = H5Dopen(hdf5_grp, "SmoothingLength", dapl_id);
  H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, dummy_float);
  H5Dclose(hdf5_dataset);
  for (n = 0; n < NumPart_ThisFile[0]; n++)
    P[n].h = dummy_float[n];
  free(dummy_float);
  if (i == 0)
    printf("P[0].h      = %f\n\n", P[0].h);

  H5Gclose(hdf5_grp);


  /* Star particles */
  if (NumPart_ThisFile[4] > 0)
    {
      hdf5_grp = H5Gopen(hdf5_file, "/PartType4", H5P_DEFAULT);

      dummy_float = (float *) calloc(NumPart_ThisFile[4], sizeof(float));
      if (NULL == dummy_float)
        {
          free(dummy_float);
          printf("Memory allocation failed in read_snapshot.c\n");
          exit(0);
        }

      /* Masses, 10^10 Msol/h */
      hdf5_dataset = H5Dopen(hdf5_grp, "Masses", dapl_id);
      H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, dummy_float);
      H5Dclose(hdf5_dataset);
      for (n = 0; n < NumPart_ThisFile[4]; n++)
        fstars += dummy_float[n];
      free(dummy_float);

      H5Gclose(hdf5_grp);
    }

  /* Black hole particles */
  if (NumPart_ThisFile[5] > 0)
    {
      hdf5_grp = H5Gopen(hdf5_file, "/PartType5", H5P_DEFAULT);

      dummy_float = (float *) calloc(NumPart_ThisFile[5], sizeof(float));
      if (NULL == dummy_float)
        {
          free(dummy_float);
          printf("Memory allocation failed in read_snapshot.c\n");
          exit(0);
        }

      /* Masses, 10^10 Msol/h */
      hdf5_dataset = H5Dopen(hdf5_grp, "Masses", dapl_id);
      H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, dummy_float);
      H5Dclose(hdf5_dataset);
      for (n = 0; n < NumPart_ThisFile[5]; n++)
        fbhs += dummy_float[n];
      free(dummy_float);

      H5Gclose(hdf5_grp);
    }

  H5Fclose(hdf5_file);

  fdark += NumPart_ThisFile[1] * mass[1];
  ftot = fdark + fgas + fstars + fbhs;

  if (i == FILENUMBER - 1)
    omegab = (fgas + fstars + fbhs) / ftot * omega0;

  return (0);
}
