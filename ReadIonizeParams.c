/* Reads in the information from TREECOOL at start of run */

#include <stdio.h>
#include <stdlib.h>

#include "proto.h"

float inlogz[TABLESIZE];
float gH0[TABLESIZE], gHe[TABLESIZE], gHep[TABLESIZE];
float eH0[TABLESIZE], eHe[TABLESIZE], eHep[TABLESIZE];
int nheattab;                   /* length of table */

void
ReadIonizeParams(char *fname)
{
  int i;
  FILE *fdcool;

  if (!(fdcool = fopen(fname, "r")))
    {
      printf(" Cannot read ionization table in file `%s'\n", fname);
      exit(0);
    }

  for (i = 0; i < TABLESIZE; i++)
    gH0[i] = 0;

  for (i = 0; i < TABLESIZE; i++)
    if (fscanf(fdcool, "%g %g %g %g %g %g %g",
               &inlogz[i], &gH0[i], &gHe[i], &gHep[i], &eH0[i], &eHe[i], &eHep[i]) == EOF)
      break;

  fclose(fdcool);

  /*  nheattab is the number of entries in the table */
  for (i = 0, nheattab = 0; i < TABLESIZE; i++)
    if (gH0[i] != 0.0)
      nheattab++;
    else
      break;

}
