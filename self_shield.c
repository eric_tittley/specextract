/* Routine to (re-)compute the neutral hydrogen fraction, with the option
   of using the self-shielding correction of Rahmati et al. 2013,
   MNRAS, 430, 2427 and Chardin et al. 2017, MNRAS submitted. */

#include <math.h>
#include "proto.h"

double
self_shield(double nHcgs, double logT, double ne_input)
{

#ifdef SELF_SHIELD

  double T4, fgas, fnH_SS, nHcgs_SS, OMEGAB0;

  OMEGAB0 = 0.0482;

  /* Optically thick threshold, Eq. 13.  Note we assume the grey
   * absorption cross-section is 2.49e-18 cm^2 (cf. Table 2) */
  T4 = pow(10.0, logT) / 1.0e4;
  fgas = OMEGAB0 / omega0;

  nHcgs_SS = 6.73e-3 * pow(T4, 0.17) * pow(gJH0 * 1.0e12, 2.0 / 3.0) * pow(fgas / 0.17, -1.0 / 3.0);

  fnH_SS = nHcgs / nHcgs_SS;

  /* Fit to RT results for 1<z<5. See Table A1, Eq. 14 and Eq. A1 */
  gJH0_SS = gJH0 * (0.98 * pow(1.0 + pow(fnH_SS, 1.64), -2.28) + 0.02 * pow(1.0 + fnH_SS, -0.84));

  /* double fnH_SS; */

  /* fnH_SS = nHcgs/n0_z; */

  /* gJH0_SS = gJH0 * ((1.0-fval_z) * pow(1.0 + pow(fnH_SS, beta_z), alpha1_z) */
  /*                + fval_z * pow(1.0 + fnH_SS, alpha2_z)); */
#endif

  ion_balance(nHcgs, logT, ne_input);

  return nH0;                   /* HI/H */
}
